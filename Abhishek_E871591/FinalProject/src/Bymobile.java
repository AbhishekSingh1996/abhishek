

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Bymobile extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public Bymobile() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		 response.setContentType("text/html");
	
		String phoneno=request.getParameter("mobile");
		int balance=Integer.parseInt(request.getParameter("amount"));
		 try {
			 Class.forName("oracle.jdbc.driver.OracleDriver");
				Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe", "system", "tiger");
			 PreparedStatement pstmt = conn.prepareStatement("update account set amount=amount+? where mobile=?");
			 pstmt.setInt(1,balance);  
			 pstmt.setString(2,phoneno);
	            
	            pstmt.executeUpdate();
	           out.print("<html><head><script>alert('Amount Send Successfully')</script></head></html>");
	           RequestDispatcher rd=request.getRequestDispatcher("function.html");
	           rd.include(request,response);	        	

	        }catch(Exception e)
	        {
	        	System.out.println(e); 
	        }
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
