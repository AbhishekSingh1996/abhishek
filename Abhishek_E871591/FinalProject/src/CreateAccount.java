

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CreateAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CreateAccount() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String id=request.getParameter("id");
		String name=request.getParameter("name");
		String acc=request.getParameter("account");
		String ifsc=request.getParameter("ifsc");
		String mob=request.getParameter("mobile");
		String am=request.getParameter("amount");
		try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe", "system", "tiger");
            PreparedStatement psmt = conn.prepareStatement("insert into account values(?,?,?,?,?,?)");
            psmt.setString(1, id);
            psmt.setString(2, name);
            psmt.setString(3, acc);
            psmt.setString(4, ifsc);
            psmt.setString(5, mob);
            psmt.setString(6, am);
            psmt.execute();
	           out.print("<html><head><script>alert('Account Created Successfully')</script></head></html>");

            RequestDispatcher requestDispatcher=request.getRequestDispatcher("function.html");
			requestDispatcher.include(request,response);
		}catch(Exception e)
		{
			System.out.println(e);
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
