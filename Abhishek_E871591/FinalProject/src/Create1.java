

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class Create1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public Create1() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		String id=request.getParameter("id");
		String name=request.getParameter("name");
		String age=request.getParameter("age");
		String salary=request.getParameter("salary");
		String desig=request.getParameter("desig");
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");  
		   LocalDateTime now = LocalDateTime.now();  
		     
		
		try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe", "system", "tiger");
            PreparedStatement psmt = conn.prepareStatement("insert into employee values(?,?,?,?,?,?)");
            psmt.setString(1, id);
            psmt.setString(2, name);
            psmt.setString(3, age);
            psmt.setString(4, salary);
            psmt.setString(5, desig);
            psmt.setString(6, dtf.format(now));
            psmt.execute();
			RequestDispatcher rd=request.getRequestDispatcher("Main.html");
			rd.include(request, response);
		}catch(Exception e)
		{
			
		}
	}
	private int parseInt(String parameter) {
		// TODO Auto-generated method stub
		return 0;
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
