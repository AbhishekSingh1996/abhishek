

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String name = request.getParameter("name");
		String password = request.getParameter("pass");
		String username1 = null;
		String password1 = null;
		response.setContentType("text/html");
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe", "system", "tiger");
			PreparedStatement psmt = conn.prepareStatement("select * from login");
			ResultSet rs = psmt.executeQuery();

			while (rs.next()) {
				String userCount = rs.getString(1);
				String passCount = rs.getString(2);
				if (userCount.equalsIgnoreCase(name) && passCount.equalsIgnoreCase(password)) {
					username1=userCount;
					password1=passCount;
					 
				}

			}
		} catch (Exception e) {
			System.out.println(e);
		}
		
		
		if(name.equals(username1)&& password.equals(password1))
		{
			RequestDispatcher requestDispatcher=request.getRequestDispatcher("Main.html");
			requestDispatcher.forward(request,response);
		 
		 }
		else {
			out.print("<p style=\"margin-top: 6%;margin-left: 43%;font-size: 20px;color: red\"><i>invalid username or password</i></p>\r\n");
			RequestDispatcher requestDispatcher=request.getRequestDispatcher("Hello.jsp");
			requestDispatcher.include(request,response);
			
			}

	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
