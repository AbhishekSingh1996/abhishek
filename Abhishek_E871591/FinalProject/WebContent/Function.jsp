<%@ page import="java.sql.*" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
 .login-box{
            width: 280px;
            margin-top:1%;
            margin-left:79%;
            transform: translate(-50%,-50%);
            color : solid black;
        }

        .textbox{
            width: 100%;
            overflow: hidden;
            font-size: 20px;
            padding: 8px 0;
            margin: 8px 0;
            border-bottom: 1px solid black;
        }
        .textbox i{
            width: 26px;
            float: left;
            text-align: center;
        }
        .textbox input{
            border: none;
            outline: none;
            background: none;
            color: black;
            font-size: 18px;
            width: 80%;
            float: left;
            margin: 0 10px;
        }
        .btn{
            width: 100%;
            background: none;
            border: 2px solid blak;
            color: black;
            padding: 5px;
            font-size: 18px;
            cursor: pointer;
            margin: 12px 0;
        }
        button:hover,
button:focus
{
    background: gray;
    transition: 0.2s ease;
}
button
{
    margin-bottom: 28px;
    width: 150px;
    height: 40px;
    background: lime;
    border: none;
    border-radius: 2px;
    color: #fff;
    font-family: sans-serif;
    font-weight: 800;
    text-transform: uppercase;
    transition: 0.2s ease;
    cursor: pointer;
    
}

button:hover,
button:focus
{
    background: #ff5722;
    transition: 0.2s ease;
}
</style>


</head>
<body>
<div  style="margin-top: 5%;margin-left: 10%">
<div  style="margin-left: 55%;margin-top: 5%;font-size: 30px">
	 <button><a href="Create.jsp" target="third">Insert</a></button>
</div>
		<%
	try {

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe", "system", "tiger");
        Statement statement=conn.createStatement();
        ResultSet resultSet=statement.executeQuery("select * from employee");
        %>
        
        <table width=45% border=1 bgcolor="skyblue" >
         <tr bgcolor="gray" style="font-size: 25px">
         <th>Id</th>
         <th>Name</th>
         <th>Age</th>
         <th>Salary</th>
         <th>Desig</th>
         <th>DateOfJoining</th>
         <th>Update</th>
         <th>Delete</th>
         <tr>
         
         <%
        
        while (resultSet.next())
        {
        	%>
        	
        	<tr style="font-size: 20px" bgcolor="white">
        	<td><%=resultSet.getString(1) %></td>
        	<td><%=resultSet.getString(2)%></td>
			<td><%=resultSet.getInt(3)%></td>
			<td><%=resultSet.getString(4)%></td>
			<td><%=resultSet.getString(5)%></td>
			<td><%=resultSet.getString(6)%></td>
			<td><a href="Update.jsp?id=<%=resultSet.getString(1)%>" target="third">Edit</a></td>
			<td><a href="Delete.jsp?id=<%=resultSet.getString(1)%>">Delete</a></td>
			</tr> 
			      
            <% 
        }

    }catch (Exception e)
    {
        System.out.println(e);
    }
	%>

		
</div >



</body>
</html>