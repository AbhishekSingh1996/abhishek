<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="javax.swing.JOptionPane"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
 
 <form >
 <%
 String id=request.getParameter("id");
 String name=request.getParameter("name");
 String age=request.getParameter("age");
 String desig=request.getParameter("desig");
 String sal=request.getParameter("salary");
 try {
 	 Class.forName("oracle.jdbc.driver.OracleDriver");
     Connection  conn= DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/XE","system","tiger");
     PreparedStatement pstmt=conn.prepareStatement("update employee set name=?,age=?,desig=?,salary=? where id=?");
     pstmt.setString(1,name);
     pstmt.setString(2,age);
     pstmt.setString(3,desig);
     pstmt.setString(4,sal);
     pstmt.setString(5,id);
     pstmt.executeUpdate();
     JOptionPane.showMessageDialog(null,"Data Updated Successfully");
     %>
     
 	<jsp:include page="Main.html">
 	<jsp:param value="Raj" name="user"/>
 	</jsp:include>
 	<% 
 }
 catch(Exception e){
	 System.out.print(e);
 }
 %>
 </form>
	
</body>
</html>