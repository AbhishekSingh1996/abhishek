<%@ page import="java.sql.*" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

<style type="text/css">
 .login-box{
            width: 280px;
            transform: translate(-50%,-50%);
            color : solid black;
            
        }

        .textbox{
            width: 100%;
            overflow: hidden;
            font-size: 30px;
            color: white;
            padding: 8px 0;
            margin: 8px 0;
            border-bottom: 1px solid black;
        }
        .textbox i{
            width: 26px;
            float: left;
            text-align: center;
        }
        .textbox input{
            border: none;
            outline: none;
            background: none;
            color: white;
            font-size: 18px;
            width: 80%;
            float: left;
            margin: 0 10px;
        }
        .btn{
            width: 100%;
            background: none;
            border: 2px solid blak;
            color: black;
            padding: 5px;
            font-size: 18px;
            cursor: pointer;
            margin: 12px 0;
        }
        input[type="submit"]:hover,
input[type="submit"]:focus
{
    background: gray;
    transition: 0.2s ease;
}
input[type="submit"]
{
    margin-bottom: 28px;
    width: 150px;
    height: 40px;
    background: lime;
    border: none;
    border-radius: 2px;
    color: #fff;
    font-family: sans-serif;
    font-weight: 800;
    text-transform: uppercase;
    transition: 0.2s ease;
    cursor: pointer;
    
}

input[type="submit"]:hover,
input[type="submit"]:focus
{
    background: #ff5722;
    transition: 0.2s ease;
}
</style>


</head>
<body>
<%	
String id1=request.getParameter("id");
try {
	Class.forName("oracle.jdbc.driver.OracleDriver");
    Connection  conn= DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/XE","system","tiger");
    PreparedStatement pstmt=conn.prepareStatement("select * from employee where id=?");
    pstmt.setString(1, id1);
    ResultSet resultSet=pstmt.executeQuery();
    while (resultSet.next())
    {
    	%>
    	<div  class="login-box" style="margin-left: 40%;margin-top: 50%">
    	<form action="Updatea.jsp" target="_top">
        <div class="textbox">
            <input type="number" name="id" value="<%=resultSet.getString(1)%>"  min="100" max="999" required>
        </div>
        <div class="textbox">
            <input type="text" name="name" value="<%=resultSet.getString(2)%>"  minlength="3" required>
        </div>
        <div class="textbox">
            <input type="number" name="age" value="<%=resultSet.getString(3)%>"  min="18" max="60" required>
        </div>
        <div class="textbox">
            <input type="number" name="salary" value="<%=resultSet.getString(4)%>"  min="25000" required>
        </div>
        <div >
            <select name="desig" value="<%=resultSet.getString(5)%>" style="height: 250%;width: 100%;font-size: 20px;background: none" required>
                <option>Select The Designation</option>
                <option>Clerk</option>
                <option>Programmer</option>
                <option>Tester</option>
                <option>Manager</option>
            </select>
        </div>

        <input type="submit" class="btn" value="Update">
    </form> 
    </div>
		      
        <% 
    }

}catch (Exception e)
{
    System.out.println(e);
}
%>


</body>
</html>